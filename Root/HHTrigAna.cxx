#include "HHTrigAna/HHTrigAna.h"

// SusyNtuple
#include "SusyNtuple/KinematicTools.h"
#include "SusyNtuple/SusyDefs.h"
using namespace Susy; // everything in SusyNtuple is in this namespace

//ROOT

// std/stl
#include <iomanip> // setw
#include <iostream>
#include <string>
#include <sstream> // stringstream, ostringstream
using namespace std;

//////////////////////////////////////////////////////////////////////////////
HHTrigAna::HHTrigAna() :
    m_dbg(0),
    m_input_chain(nullptr),
    m_mc_weight(1.0),
    _rfile(nullptr),
    _rtree(nullptr)
{
}
//////////////////////////////////////////////////////////////////////////////
void HHTrigAna::Begin(TTree* /*tree*/)
{
    // call base class' Begin method
    SusyNtAna::Begin(0);
    if(dbg()) cout << "HHTrigAna::Begin" << endl;

    _trig_tool = std::make_unique<dileptrig::DileptonTriggerTool>();
    _trig_tool->initialize("WWBB15161718RAND", &nttools().triggerTool());

    setup_output();

    return;
}
//////////////////////////////////////////////////////////////////////////////
void HHTrigAna::setup_output()
{
    _rfile = new TFile("hh_trig_ana.root", "RECREATE");
    _rtree = new TTree("trig", "trig");

    _rtree->Branch("year", &b_year);
    _rtree->Branch("leptons_truth_matched", &b_leptons_truth_matched);
    _rtree->Branch("l0_truth_matched", &b_l0_truth_matched);
    _rtree->Branch("l1_truth_matched", &b_l1_truth_matched);
    _rtree->Branch("pass_trigger", &b_pass_trigger);
    _rtree->Branch("isEE", &b_isEE);
    _rtree->Branch("isMM", &b_isMM);
    _rtree->Branch("isEM", &b_isEM);
    _rtree->Branch("isME", &b_isME);
    _rtree->Branch("l0_pt", &b_l0_pt);
    _rtree->Branch("l1_pt", &b_l1_pt);
    _rtree->Branch("l0_eta", &b_l0_eta);
    _rtree->Branch("l1_eta", &b_l1_eta);
    _rtree->Branch("l0_phi", &b_l0_phi);
    _rtree->Branch("l1_phi", &b_l1_phi);

}
//////////////////////////////////////////////////////////////////////////////
Bool_t HHTrigAna::Process(Long64_t entry)
{

    // calling "GetEntry" loads into memory the susyNt class objects for this event
    GetEntry(entry);
    SusyNtAna::clearObjects(); // clear the previous event's objects

    // increment the chain entry (c.f. SusyNtuple/SusyNtAna.h)
    m_chainEntry++;

    // evt() provides pointer to the SusyNt::Event class object for this event
    int run_number = nt.evt()->run;
    int event_number = nt.evt()->eventNumber;

    if(dbg() || m_chainEntry%1000==0) {
        cout << "HHTrigAna::Process    **** Processing entry " << setw(6) << m_chainEntry
                << "  run " << run_number << "  event " << event_number << " **** " << endl;
    }

    // SusyNtAna::selectObject fills the baseline and signal objects
    // for the given AnalysisType
    // m_preX    = objects before any selection (as they are in susyNt)
    // m_baseX   = objects with the Analysis' baseline selection AND overlap removal applied
    // m_signalX = objects with the Analysis' signal selection applied (and baseline AND overlap removal)
    SusyNtAna::selectObjects();

    // get the MC weight using the inherited MCWeighter object
    // (c.f. SusyNtuple/MCWeighter.h)
    if(nt.evt()->isMC) {
        float lumi = 100000; // normalize the MC to 100 fb-1
        m_mc_weight = SusyNtAna::mcWeighter().getMCWeight(nt.evt(), lumi, NtSys::NOM, /*multi-period*/ false, /*include PRW*/ true);
    }
    else {
        m_mc_weight = 1.; // don't re-weight data
    }

    // check that the event passes the standard ATLAS event cleaning cuts
    if(!passEventCleaning(m_preMuons, m_baseMuons, m_baseJets)) return false;

    int n_leptons = m_signalLeptons.size();
    if(!(n_leptons==2)) return kTRUE;

    check_triggers();

    return kTRUE;
}
//////////////////////////////////////////////////////////////////////////////
bool HHTrigAna::passEventCleaning(const MuonVector& preMuons, const MuonVector& baseMuons,
            const JetVector& baseJets)
{
    int flags = nt.evt()->cutFlags[NtSys::NOM];

    if(!nttools().passGRL(flags))           return false;

    if(!nttools().passLarErr(flags))        return false;

    if(!nttools().passTileErr(flags))       return false;

    if(!nttools().passTTC(flags))           return false;

    if(!nttools().passSCTErr(flags))        return false;

    if(!nttools().passGoodVtx(flags))       return false;


    ///////////////////////////////////////////////////////
    // for bad muon, cosmic moun, and jet cleaning the
    // cuts depend on the baseline object defintion
    // (and in thec ase of the cosmic muon cut, it also
    // depends on the analysis' overlap removal
    // procedure) -- so we do not use the cutFlags but
    // rather use the objects that have passed the various
    // analysis selections to do the checks
    ///////////////////////////////////////////////////////
    if(!nttools().passBadMuon(preMuons))    return false;

    if(!nttools().passCosmicMuon(baseMuons)) return false;

    if(!nttools().passJetCleaning(baseJets)) return false;

    return true;
}
//////////////////////////////////////////////////////////////////////////////
void HHTrigAna::check_triggers()
{
    b_year = nt.evt()->treatAsYear;
    auto leptons = m_signalLeptons;
    auto l0 = leptons.at(0);
    auto l1 = leptons.at(1);

    b_isEE = (l0->isEle() && l1->isEle()) ? 1 : 0;
    b_isMM = (l0->isMu() && l1->isMu()) ? 1 : 0;
    b_isEM = (l0->isEle() && l1->isMu()) ? 1 : 0;
    b_isME = (l0->isMu() && l1->isEle()) ? 1 : 0;

    bool l0_truth_matched = l0->matched2TruthLepton;
    bool l1_truth_matched = l1->matched2TruthLepton;
    b_l0_truth_matched = (l0_truth_matched ? 1 : 0);
    b_l1_truth_matched = (l1_truth_matched ? 1 : 0);
    b_leptons_truth_matched = ((l0_truth_matched && l1_truth_matched) ? 1 : 0);

    b_l0_pt = l0->Pt();
    b_l1_pt = l1->Pt();
    b_l0_eta = l0->Eta();
    b_l1_eta = l1->Eta();
    b_l0_phi = l0->Phi();
    b_l1_phi = l1->Phi();

    b_pass_trigger = (_trig_tool->pass_trigger_selection(nt.evt(), l0, l1, /*require matching*/ false) ? 1 : 0);

    // fill tree
    _rtree->Fill();
}
//////////////////////////////////////////////////////////////////////////////
void HHTrigAna::Terminate()
{
    // close SusyNtAna and print timers
    SusyNtAna::Terminate();

    _rfile->cd();
    _rtree->Write();
    _rfile->Write();

    return;
}
//////////////////////////////////////////////////////////////////////////////
