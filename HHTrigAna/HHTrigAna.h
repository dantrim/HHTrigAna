#ifndef SusyNtuple_HHTrigAna_h
#define SusyNtuple_HHTrigAna_h

//ROOT
#include "TTree.h"
#include "TChain.h"
#include "TFile.h"

//SusyNtuple
#include "SusyNtuple/SusyNtAna.h"
#include "SusyNtuple/SusyNtTools.h"

//Trigger
#include "DileptonTriggerTool/DileptonTriggerTool.h"

//std/stl
#include <fstream>
#include <memory>

/////////////////////////////////////////////////////////////
//
// HHTrigAna
// Class auto-generated with SusyNtuple/make_susy_skeleton on 2019-02-07 10:39
//
/////////////////////////////////////////////////////////////

// for TSelector analysis loopers processing susyNt you MUST inherit from SusyNtAna
// in order to pick up the susyNt class objects
class HHTrigAna : public SusyNtAna
{

    public :
        HHTrigAna();
        virtual ~HHTrigAna() {};

        void set_debug(int dbg) { m_dbg = dbg; }
        int dbg() { return m_dbg; }

        void set_chain(TChain* chain) { m_input_chain = chain; }
        TChain* chain() { return m_input_chain; }

        ////////////////////////////////////////////
        // analysis methods
        ////////////////////////////////////////////

        // standard ATLAS event cleaning
        bool passEventCleaning(const MuonVector& preMuons, const MuonVector& baseMuons,
                const JetVector& baseJets);

        void setup_output();
        void check_triggers();

        ////////////////////////////////////////////
        // TSelector methods override
        ////////////////////////////////////////////
        virtual void Begin(TTree* tree); // Begin is called before looping on entries
        virtual Bool_t Process(Long64_t entry); // Main event loop function called on each event
        virtual void Terminate(); // Terminate is called after looping has finished
        TTree* output_tree() { return _rtree; }
        TFile* output_file() { return _rfile; }


    private :
        int m_dbg;
        TChain* m_input_chain; // the TChain object we are processing
        float m_mc_weight;

        std::unique_ptr<dileptrig::DileptonTriggerTool> _trig_tool;

        // output
        TFile* _rfile;
        TTree* _rtree;

        // branches
        int b_year;
        int b_leptons_truth_matched;
        int b_l0_truth_matched;
        int b_l1_truth_matched;
        int b_pass_trigger;
        int b_isEE;
        int b_isMM;
        int b_isEM;
        int b_isME;
        float b_l0_pt;
        float b_l1_pt;
        float b_l0_eta;
        float b_l1_eta;
        float b_l0_phi;
        float b_l1_phi;

        

}; //class


#endif
