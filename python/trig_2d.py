#!/bin/env python

from __future__ import print_function
import sys, os, argparse
import ROOT as r
#r.gROOT.SetBatch(True)
r.gStyle.SetOptStat(0)

def make_plot(args): 

    c = r.TCanvas("c", "", 800, 600)
    c.SetGrid(1,1)
    c.SetTicks(1,1)
    c.cd()

    rfile = r.TFile.Open(args.input)
    tree = rfile.Get("trig")

    x_bounds = [80, -1.2, 1.2]
    y_bounds = [100, -3, 3]

    #x_bounds = [50, 10, 250]
    #y_bounds = [100, -1.5, 1.5]

    h2_den = r.TH2F("h2_den", "", x_bounds[0], x_bounds[1], x_bounds[2], y_bounds[0], y_bounds[1], y_bounds[2]) 
    h2_num = r.TH2F("h2_num", "", x_bounds[0], x_bounds[1], x_bounds[2], y_bounds[0], y_bounds[1], y_bounds[2]) 

    tree.Draw("l0_phi:l0_eta>>h2_den", "isME==1 && leptons_truth_matched==1", "goff")
    tree.Draw("l0_phi:l0_eta>>h2_num", "isME==1 && leptons_truth_matched==1 && pass_trigger==1", "goff")

    h2_num.Divide(h2_den)
    r.gPad.SetRightMargin(1.25 * r.gPad.GetRightMargin())

    xax = h2_num.GetXaxis()
    yax = h2_num.GetYaxis()
    zax = h2_num.GetZaxis()


    xax.SetTitle("Muon #eta")
    yax.SetTitle("Muon #phi")
    zax.SetTitle("Efficiency to Pass Trigger")

    h2_num.Draw("colz")

    c.Update()
    x = raw_input()
    c.SaveAs("wwbb_2d_trig_eff_EM_AfterFix.pdf")
    #c.SaveAs("wwbb_2d_trig_eff_EM_Pt_AfterFix.pdf")

def main() :

    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', required = True,
        help = 'Provide input trig file'
    )
    args = parser.parse_args()

    if not os.path.isfile(args.input) :
        print("ERROR Provided input file ({}) not found".format(args.input))
        sys.exit()

    make_plot(args)

if __name__ == '__main__' :
    main()
