#!/bin/env python

from __future__ import print_function
import sys, os, argparse
import ROOT as r
r.gROOT.SetBatch(True)
r.gStyle.SetOptStat(0)
r.PyConfig.IgnoreCommandLineOptions = True
r.TH1F._creates = False

class Blah :
    def __init__(self) :
        self.histo = None

def var_bounds() :

    out = {}
    out['l0_pt'] = [ [25, 10, 115], 'Leading lepton p_{T} [GeV]' ]
    out['l1_pt'] = [ [25, 5, 65], 'Sub-leading lepton p_{T} [GeV]' ]
    out['l0_eta'] = [ [25, -2.5, 2.5], 'Leading lepton #eta' ]
    out['l1_eta'] = [ [25, -2.5, 2.5], 'Sub-leading lepton #eta' ]
  #  out['l0_phi'] = [ [20, -3.2, 3.2], 'Leading lepton #phi' ]
  #  out['l1_phi'] = [ [20, -3.2, 3.2], 'Sub-leading lepton #phi' ]
    return out

def make_plot(flavor_string, args) :

    filename = args.input
    rfile = r.TFile.Open(filename)
    tree = rfile.Get("trig")

    variables = var_bounds()
    for varname in variables :

        x_bounds = variables[varname][0]
        x_label = variables[varname][1]

        c = r.TCanvas("c_%s_%s" % (flavor_string, varname), "", 800, 600)
        c.SetGrid(1,1)
        c.SetTicks(1,1)
        c.cd()

        h_num = r.TH1F("h_%s_%s_num" % (flavor_string, varname), "", x_bounds[0], x_bounds[1], x_bounds[2])
        h_num.Sumw2()
        h_den = r.TH1F("h_%s_%s_den" % (flavor_string, varname), "", x_bounds[0], x_bounds[1], x_bounds[2])
        h_den.Sumw2()

        for h in [h_num, h_den] :

            xaxis = h.GetXaxis()
            yaxis = h.GetYaxis()

            xaxis.SetTitle(x_label)
            yaxis.SetTitle("#epsilon")

        if flavor_string != "ALL" :
            flavor_cut = "%s==1" % flavor_string
        else :
            flavor_cut = "(isEE==1 || isMM==1 || isEM==1 || isME==1)"
        do_year = "2018"
        cut_den = "year==%s && %s && leptons_truth_matched==1" % (do_year,flavor_cut)
        cut_num = "year==%s && %s && pass_trigger==1" % (do_year, cut_den)

        tree.Draw("%s>>%s" % (varname, h_den.GetName()), cut_den, "goff")
        tree.Draw("%s>>%s" % (varname, h_num.GetName()), cut_num, "goff")

        h_num.Divide(h_den)
        h_num.SetLineWidth(2)

        h_num.SetLineColor(r.kBlack)
        h_num.SetMarkerStyle(20)
        h_num.SetMaximum(1.4)
        h_num.SetMinimum(0)


        h_num.Draw("hist")
        c.Update()

        line = r.TLine(x_bounds[1], 1.0, x_bounds[2], 1.0)
        line.SetLineColor(r.kRed)
        line.SetLineWidth(2)
        line.SetLineStyle(2)
        line.Draw()
        c.Update()

        c.SaveAs("%s_%s.eps" % (c.GetName(), do_year))

def make_comp_plots(flavor_string, args) :

    filename_double_noasym = "./samples/hh_trig_ana_Combined_DoubleOnlyNoAsym.root"
    filename_double = "./samples/hh_trig_ana_Combined_DoubleOnly.root"
    filename_note = "./samples/hh_trig_ana_Combined_AsInNote.root"
    filename_fix = "./samples/hh_trig_ana_Combined_CheckBothSingle.root"

    rfile_double_noasym = r.TFile.Open(filename_double_noasym)
    rfile_double = r.TFile.Open(filename_double)
    rfile_note = r.TFile.Open(filename_note)
    rfile_fix = r.TFile.Open(filename_fix)

    tree_double_noasym = rfile_double_noasym.Get("trig")
    tree_double = rfile_double.Get("trig")
    tree_note = rfile_note.Get("trig")
    tree_fix = rfile_fix.Get("trig")

    #colors = [r.kRed, r.kMagenta, r.kBlack, r.kGreen]
    colors = [r.TColor.GetColor("#ec4e20"), r.TColor.GetColor("#ff9505"), r.TColor.GetColor("#353531"), r.TColor.GetColor("#0165b9")]
    names = ["double_noasym", "double", "note", "fix"]
    nice_names = ["Dilepton Only (w/o Asymm.)", "Dilepton Only (w/ Asymm.)", "Version <= v0.4", "Version > v0.4"]
    trees = [tree_double_noasym, tree_double, tree_note, tree_fix]

    do_year = "2018"

    variables = var_bounds()
    for varname in variables :

        eff_histos = []

        x_bounds = variables[varname][0]
        x_label = variables[varname][1]

        if do_year == "2015" :
            x_bounds[0] = int(0.5 * x_bounds[0])

        c = r.TCanvas("c_eff_%s_%s.eps" % (varname, flavor_string), "", 800, 600)
        c.SetGrid(1,1)
        c.SetTicks(1,1)
        c.cd()
        axis = r.TH1F("axis_%s_%s" % (varname, flavor_string), "", x_bounds[0], x_bounds[1], x_bounds[2])
        axis.Draw("axis")

        leg = r.TLegend(0.5, 0.14, 0.88, 0.35)

        for itree, tree in enumerate(trees) :
        #input_files = [filename_double_noasym, filename_double, filename_note, filename_fix]
        #for itree, filename in enumerate(input_files) :

            #print("opening %s" % filename)
            #rfile = r.TFile.Open(filename)
            #tree = rfile.Get("trig")

            h_num = r.TH1F("h_%s_%s_%s_num" % (flavor_string, varname, names[itree]), "", x_bounds[0], x_bounds[1], x_bounds[2])
            h_num.Sumw2()
            h_den = r.TH1F("h_%s_%s_%s_den" % (flavor_string, varname, names[itree]), "", x_bounds[0], x_bounds[1], x_bounds[2])
            h_den.Sumw2()
            for h in [h_num, h_den] :
                xaxis = h.GetXaxis()
                yaxis = h.GetYaxis()
                xaxis.SetTitle(x_label)
                yaxis.SetTitle("Efficiency to Pass Trigger Selection")

            if flavor_string != "ALL" :
                flavor_cut = "%s==1" % flavor_string
            else :
                flavor_cut = "(isEE==1 || isMM==1 || isEM==1 || isME==1)"
            cut_den = "year==%s && %s && leptons_truth_matched==1" % (do_year, flavor_cut)
            cut_num = "year==%s && %s && pass_trigger==1" % (do_year, cut_den)

            tree.Draw("%s>>%s" % (varname, h_den.GetName()), cut_den, "goff")
            tree.Draw("%s>>%s" % (varname, h_num.GetName()), cut_num, "goff")

            h_ratio = h_num.Clone("%s_ratio" % h_num.GetName())
            h_num.Divide(h_den)
            if "fix" in names[itree] or names[itree] == "double" :
                h_num.Scale(1.003)
            r.SetOwnership(h_num, False)
            cmd = "hist"
            if itree > 0 : cmd += " same"
            h_num.SetLineColor(colors[itree])
            h_num.SetMaximum(1.2)
            h_num.SetMinimum(0.4)
            h_num.SetLineWidth(2)
            c.cd()
            h_num.Draw(cmd)
            c.Update()
            leg.AddEntry(h_num, nice_names[itree], "l")

        #for ihist, hist in enumerate(eff_histos) :
        #    print(hist.hist)
        #    print("Eff histo name %s" % hist.hist.GetName())
        #    cmd = "hist"
        #    if ihist > 0 : cmd += " same"
        #    hist.hist.SetLineColor(colors[ihist])
        #    #h.SetLineWidth(2)
        #    #h.SetMaximum(1.4)
        #    #h.SetMinimum(0)
        #    h.Draw(cmd)
        #    c.Update()
        leg.Draw()
        c.Update()
        line = r.TLine(x_bounds[1], 1.0, x_bounds[2], 1.0)
        line.SetLineColor(r.kRed)
        line.SetLineWidth(2)
        line.SetLineStyle(2)
        line.Draw()
        c.Update()

        text = r.TLatex()
        text.SetTextFont(42)
        text.SetNDC()
        text.DrawLatex(0.13, 0.83, "#it{#bf{ATLAS}} Simulation Internal")
        text.SetTextSize(0.8 * text.GetTextSize())
        c.Update()
        text.DrawLatex(0.13, 0.79, "#it{hh #rightarrow WWbb, 2l}")
        c.Update()

        c.SaveAs("%s_%s_comb.eps" % (c.GetName(), do_year))


def make_plots(args) :

    flavors = ['isEE', 'isMM', 'isEM', 'isME', 'ALL']

    for flav in flavors :
#        make_plot(flav, args)
        make_comp_plots(flav, args)

def main() :

    parser = argparse.ArgumentParser(description = 'Plot trigger efficiency plots')
    parser.add_argument('-i', '--input', required = True,
        help = 'Input file'
    )
    parser.add_argument('-s', '--suffix', default = '',
        help = 'Add a suffix to any of the outputs'
    )
    args = parser.parse_args()

    if not os.path.isfile(args.input) :
        print("ERROR Input file (%s) not found" % (args.input))
        sys.exit()

    make_plots(args)

if __name__ == '__main__' :
    main()

